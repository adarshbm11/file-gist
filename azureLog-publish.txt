2023-06-19T12:23:06.6353037Z ##[section]Starting: Generate Antora site
2023-06-19T12:23:06.6356557Z ==============================================================================
2023-06-19T12:23:06.6356666Z Task         : Command line
2023-06-19T12:23:06.6356738Z Description  : Run a command line script using Bash on Linux and macOS and cmd.exe on Windows
2023-06-19T12:23:06.6356838Z Version      : 2.212.0
2023-06-19T12:23:06.6356891Z Author       : Microsoft Corporation
2023-06-19T12:23:06.6356954Z Help         : https://docs.microsoft.com/azure/devops/pipelines/tasks/utility/command-line
2023-06-19T12:23:06.6357048Z ==============================================================================
2023-06-19T12:23:06.7808813Z Generating script.
2023-06-19T12:23:06.7823034Z ========================== Starting Command Output ===========================
2023-06-19T12:23:06.7863031Z [command]/usr/bin/bash --noprofile --norc /home/vsts/work/_temp/07b40cdc-b0e9-4801-bd81-439bd439b6df.sh
2023-06-19T12:23:11.2629323Z npm WARN deprecated core-js@3.6.5: core-js@<3.23.3 is no longer maintained and not recommended for usage due to the number of issues. Because of the V8 engine whims, feature detection in old core-js versions could cause a slowdown up to 100x even if nothing is polyfilled. Some versions have web compatibility issues. Please, upgrade your dependencies to the actual version of core-js.
2023-06-19T12:23:11.4830609Z npm WARN deprecated asciidoctor.js@1.5.9: Package no longer supported. Replaced by @asciidoctor/core
2023-06-19T12:23:11.7268528Z npm WARN deprecated docsearch.js@2.6.3: This package has been deprecated and is no longer maintained. Please use @docsearch/js.
2023-06-19T12:23:12.4143011Z npm WARN deprecated highlight.js@9.18.3: Version no longer supported. Upgrade to @latest
2023-06-19T12:23:12.4835382Z npm WARN deprecated gulp-vinyl-zip@2.2.1: Package no longer supported. Contact Support at https://www.npmjs.com/support for more info.
2023-06-19T12:23:15.6476254Z npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
2023-06-19T12:23:16.5029349Z npm WARN deprecated gulp-util@3.0.8: gulp-util is deprecated - replace it, following the guidelines at https://medium.com/gulpjs/gulp-util-ca3b1f9f9ac5
2023-06-19T12:23:17.6697289Z npm WARN deprecated @stylelint/postcss-css-in-js@0.37.3: Package no longer supported. Contact Support at https://www.npmjs.com/support for more info.
2023-06-19T12:23:17.9262811Z npm WARN deprecated @stylelint/postcss-markdown@0.36.2: Use the original unforked package instead: postcss-markdown
2023-06-19T12:23:22.6436890Z npm WARN deprecated chokidar@2.1.8: Chokidar 2 does not receive security updates since 2019. Upgrade to chokidar 3 with 15x fewer dependencies
2023-06-19T12:23:24.8902179Z npm WARN deprecated har-validator@5.1.5: this library is no longer supported
2023-06-19T12:23:24.9822782Z npm WARN deprecated uuid@3.4.0: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
2023-06-19T12:23:25.1134654Z npm WARN deprecated mkdirp@0.3.0: Legacy versions of mkdirp are no longer supported. Please update to mkdirp 1.x. (Note that the API surface has changed to use Promises in 1.x.)
2023-06-19T12:23:25.7796626Z npm WARN deprecated svgo@1.3.2: This SVGO version is no longer supported. Upgrade to v2.x.x.
2023-06-19T12:23:27.2201398Z npm WARN deprecated gulp-vinyl-zip@2.5.0: Package no longer supported. Contact Support at https://www.npmjs.com/support for more info.
2023-06-19T12:23:28.0784242Z npm WARN deprecated fsevents@1.2.13: The v1 package contains DANGEROUS / INSECURE binaries. Upgrade to safe fsevents v2
2023-06-19T12:23:29.8910427Z npm WARN deprecated sourcemap-codec@1.4.8: Please use @jridgewell/sourcemap-codec instead
2023-06-19T12:23:30.9696428Z npm WARN deprecated stable@0.1.8: Modern JS already guarantees Array#sort() is a stable sort, so this library is deprecated. See the compatibility table on MDN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#browser_compatibility
2023-06-19T12:23:32.9166823Z npm WARN deprecated source-map-resolve@0.5.3: See https://github.com/lydell/source-map-resolve#deprecated
2023-06-19T12:23:34.1940523Z npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
2023-06-19T12:23:34.2187116Z npm WARN deprecated source-map-url@0.4.1: See https://github.com/lydell/source-map-url#deprecated
2023-06-19T12:23:34.2511996Z npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
2023-06-19T12:24:00.0080554Z 
2023-06-19T12:24:00.0084068Z > es5-ext@0.10.62 postinstall /home/vsts/work/1/s/node_modules/es5-ext
2023-06-19T12:24:00.0084946Z >  node -e "try{require('./_postinstall')}catch(e){}" || exit 0
2023-06-19T12:24:00.0085533Z 
2023-06-19T12:24:00.2000290Z 
2023-06-19T12:24:00.2001429Z > gifsicle@4.0.1 postinstall /home/vsts/work/1/s/node_modules/gifsicle
2023-06-19T12:24:00.2002163Z > node lib/install.js
2023-06-19T12:24:00.2002525Z 
2023-06-19T12:24:00.8023831Z   ✔ gifsicle pre-build test passed successfully
2023-06-19T12:24:00.8129199Z 
2023-06-19T12:24:00.8130331Z > jpegtran-bin@4.0.0 postinstall /home/vsts/work/1/s/node_modules/jpegtran-bin
2023-06-19T12:24:00.8130932Z > node lib/install.js
2023-06-19T12:24:00.8131193Z 
2023-06-19T12:24:01.5638975Z   ✔ jpegtran pre-build test passed successfully
2023-06-19T12:24:01.5715445Z 
2023-06-19T12:24:01.5716503Z > optipng-bin@6.0.0 postinstall /home/vsts/work/1/s/node_modules/optipng-bin
2023-06-19T12:24:01.5717099Z > node lib/install.js
2023-06-19T12:24:01.5717347Z 
2023-06-19T12:24:02.2815983Z   ✔ optipng pre-build test passed successfully
2023-06-19T12:24:02.2991071Z 
2023-06-19T12:24:02.2992207Z > core-js@3.6.5 postinstall /home/vsts/work/1/s/node_modules/core-js
2023-06-19T12:24:02.2992908Z > node -e "try{require('./postinstall')}catch(e){}"
2023-06-19T12:24:02.2993153Z 
2023-06-19T12:24:03.2991169Z npm WARN notsup Unsupported engine for @antora/lunr-extension@1.0.0-alpha.8: wanted: {"node":">=16.0.0"} (current: {"node":"14.21.3","npm":"6.14.18"})
2023-06-19T12:24:03.2991733Z npm WARN notsup Not compatible with your version of node/npm: @antora/lunr-extension@1.0.0-alpha.8
2023-06-19T12:24:03.3027979Z npm WARN notsup Unsupported engine for @antora/pdf-extension@1.0.0-alpha.6: wanted: {"node":">=16.0.0"} (current: {"node":"14.21.3","npm":"6.14.18"})
2023-06-19T12:24:03.3028435Z npm WARN notsup Not compatible with your version of node/npm: @antora/pdf-extension@1.0.0-alpha.6
2023-06-19T12:24:03.3064486Z npm WARN notsup Unsupported engine for @asciidoctor/tabs@1.0.0-beta.5: wanted: {"node":">=16.0.0"} (current: {"node":"14.21.3","npm":"6.14.18"})
2023-06-19T12:24:03.3065291Z npm WARN notsup Not compatible with your version of node/npm: @asciidoctor/tabs@1.0.0-beta.5
2023-06-19T12:24:03.3100922Z npm WARN notsup Unsupported engine for @antora/assembler@1.0.0-alpha.6: wanted: {"node":">=16.0.0"} (current: {"node":"14.21.3","npm":"6.14.18"})
2023-06-19T12:24:03.3101624Z npm WARN notsup Not compatible with your version of node/npm: @antora/assembler@1.0.0-alpha.6
2023-06-19T12:24:03.3174631Z npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules/chokidar/node_modules/fsevents):
2023-06-19T12:24:03.3175186Z npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
2023-06-19T12:24:03.3178272Z 
2023-06-19T12:24:03.3242699Z added 1821 packages from 1011 contributors and audited 1832 packages in 53.307s
2023-06-19T12:24:03.9189271Z 
2023-06-19T12:24:03.9190272Z 173 packages are looking for funding
2023-06-19T12:24:03.9190461Z   run `npm fund` for details
2023-06-19T12:24:03.9190571Z 
2023-06-19T12:24:03.9198757Z found 21 vulnerabilities (2 low, 4 moderate, 14 high, 1 critical)
2023-06-19T12:24:03.9199025Z   run `npm audit fix` to fix them, or `npm audit` for details
2023-06-19T12:24:16.2790369Z [12:24:16.275] WARN: logger not configured; creating logger with default settings
2023-06-19T12:24:17.8320918Z [12:24:17.830] INFO (asciidoctor): possible invalid reference: 
2023-06-19T12:24:17.8321442Z     file: modules/ROOT/pages/Account_Type.adoc
2023-06-19T12:24:17.8322046Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:17.8363630Z [12:24:17.835] INFO (asciidoctor): possible invalid reference: 
2023-06-19T12:24:17.8364007Z     file: modules/ROOT/pages/Account_Type.adoc
2023-06-19T12:24:17.8364573Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:17.9025538Z [12:24:17.901] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:17.9026193Z     file: modules/ROOT/pages/Catalog.adoc:6
2023-06-19T12:24:17.9027283Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:18.4689612Z [12:24:18.468] INFO (asciidoctor): possible invalid reference: 
2023-06-19T12:24:18.4690319Z     file: modules/ROOT/pages/Deliverect_Sales_Details.adoc
2023-06-19T12:24:18.4691658Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:18.4717039Z [12:24:18.471] INFO (asciidoctor): possible invalid reference: 
2023-06-19T12:24:18.4717504Z     file: modules/ROOT/pages/Deliverect_Sales_Details.adoc
2023-06-19T12:24:18.4762632Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:18.8120456Z [12:24:18.811] INFO (asciidoctor): possible invalid reference: 
2023-06-19T12:24:18.8121105Z     file: modules/ROOT/pages/Order_Status.adoc
2023-06-19T12:24:18.8122428Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:18.8164858Z [12:24:18.815] INFO (asciidoctor): possible invalid reference: 
2023-06-19T12:24:18.8165419Z     file: modules/ROOT/pages/Order_Status.adoc
2023-06-19T12:24:18.8166409Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:18.9388669Z [12:24:18.938] ERROR (asciidoctor): target of image not found: new/image437.png
2023-06-19T12:24:18.9389394Z     file: modules/ROOT/pages/Other_Charges.adoc
2023-06-19T12:24:18.9390431Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:19.1031837Z [12:24:19.102] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:19.1032479Z     file: modules/ROOT/pages/Product.adoc:172
2023-06-19T12:24:19.1033633Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:19.1053595Z [12:24:19.104] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:19.1054183Z     file: modules/ROOT/pages/Product.adoc:201
2023-06-19T12:24:19.1054857Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:19.1073478Z [12:24:19.106] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:19.1073944Z     file: modules/ROOT/pages/Product.adoc:211
2023-06-19T12:24:19.1074664Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:19.1135195Z [12:24:19.112] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:19.1135805Z     file: modules/ROOT/pages/Product.adoc:238
2023-06-19T12:24:19.1136708Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/rest-web-sm (refname: main)
2023-06-19T12:24:20.3675762Z [12:24:20.366] ERROR (asciidoctor): target of image not found: lazeez-images/CP/Catalog-View.jpg
2023-06-19T12:24:20.3676115Z     file: modules/ROOT/pages/Catalog.adoc
2023-06-19T12:24:20.3676422Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:20.3692512Z [12:24:20.368] ERROR (asciidoctor): target of image not found: lazeez-images/CP/catalog.png
2023-06-19T12:24:20.3693257Z     file: modules/ROOT/pages/Catalog.adoc
2023-06-19T12:24:20.3693595Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.2730026Z [12:24:21.272] WARN (asciidoctor): unterminated comment block
2023-06-19T12:24:21.2730895Z     file: modules/ROOT/pages/cards.adoc:23
2023-06-19T12:24:21.2731945Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.2999538Z [12:24:21.299] ERROR (asciidoctor): target of xref not found: c.adoc
2023-06-19T12:24:21.3000919Z     file: modules/ROOT/pages/erp-faq-sample.adoc
2023-06-19T12:24:21.3003096Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.3007367Z [12:24:21.299] ERROR (asciidoctor): target of xref not found: c.adoc
2023-06-19T12:24:21.3007949Z     file: modules/ROOT/pages/erp-faq-sample.adoc
2023-06-19T12:24:21.3056553Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.3056804Z [12:24:21.301] ERROR (asciidoctor): target of xref not found: c.adoc
2023-06-19T12:24:21.3057065Z     file: modules/ROOT/pages/erp-faq-sample.adoc
2023-06-19T12:24:21.3057357Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.3057571Z [12:24:21.301] ERROR (asciidoctor): target of xref not found: c.adoc
2023-06-19T12:24:21.3057809Z     file: modules/ROOT/pages/erp-faq-sample.adoc
2023-06-19T12:24:21.3058097Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.3058316Z [12:24:21.302] ERROR (asciidoctor): target of xref not found: c.adoc
2023-06-19T12:24:21.3058553Z     file: modules/ROOT/pages/erp-faq-sample.adoc
2023-06-19T12:24:21.3058856Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.3059064Z [12:24:21.303] ERROR (asciidoctor): target of xref not found: c.adoc
2023-06-19T12:24:21.3059319Z     file: modules/ROOT/pages/erp-faq-sample.adoc
2023-06-19T12:24:21.3059608Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.3318992Z [12:24:21.330] WARN (asciidoctor): unterminated comment block
2023-06-19T12:24:21.3319260Z     file: modules/ROOT/pages/cards.adoc:23
2023-06-19T12:24:21.3319887Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/retail-web-sm (refname: main)
2023-06-19T12:24:21.3320067Z     stack:
2023-06-19T12:24:21.3320335Z         file: modules/ROOT/pages/faq-sample.adoc:60
2023-06-19T12:24:21.5931741Z [12:24:21.592] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:21.5932609Z     file: modules/ROOT/pages/Credit_Info.adoc:25
2023-06-19T12:24:21.5934062Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/quickops-sm (refname: main)
2023-06-19T12:24:21.6384452Z [12:24:21.637] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:21.6385142Z     file: modules/ROOT/pages/Day_Book_Entry.adoc:79
2023-06-19T12:24:21.6386023Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/quickops-sm (refname: main)
2023-06-19T12:24:22.1856707Z [12:24:22.184] INFO (asciidoctor): possible invalid reference: direct
2023-06-19T12:24:22.1857482Z     file: modules/ROOT/pages/Stock_receive.adoc
2023-06-19T12:24:22.1858967Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/bizops-sm (refname: main)
2023-06-19T12:24:22.1863535Z [12:24:22.185] INFO (asciidoctor): possible invalid reference: reference-document
2023-06-19T12:24:22.1864058Z     file: modules/ROOT/pages/Stock_receive.adoc
2023-06-19T12:24:22.1918003Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/bizops-sm (refname: main)
2023-06-19T12:24:22.2519097Z [12:24:22.250] INFO (asciidoctor): possible invalid reference: direct
2023-06-19T12:24:22.2519676Z     file: modules/ROOT/pages/Stock_return.adoc
2023-06-19T12:24:22.2520356Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/bizops-sm (refname: main)
2023-06-19T12:24:22.2520734Z [12:24:22.251] INFO (asciidoctor): possible invalid reference: reference-document
2023-06-19T12:24:22.2520955Z     file: modules/ROOT/pages/Stock_return.adoc
2023-06-19T12:24:22.2521280Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/bizops-sm (refname: main)
2023-06-19T12:24:22.5948246Z [12:24:22.594] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:22.5948950Z     file: modules/ROOT/pages/Day_Book_Entry.adoc:27
2023-06-19T12:24:22.5950109Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:22.6093872Z [12:24:22.606] WARN (asciidoctor): section title out of sequence: expected level 1, got level 2
2023-06-19T12:24:22.6094492Z     file: modules/ROOT/pages/Day_Book_Entry.adoc:106
2023-06-19T12:24:22.6095586Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:22.6226311Z [12:24:22.622] INFO (asciidoctor): possible invalid reference: day-book-entry
2023-06-19T12:24:22.6226884Z     file: modules/ROOT/pages/Day_Book_Entry.adoc
2023-06-19T12:24:22.6227770Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:22.6233853Z [12:24:22.622] INFO (asciidoctor): possible invalid reference: screens-day-book-entry
2023-06-19T12:24:22.6234267Z     file: modules/ROOT/pages/Day_Book_Entry.adoc
2023-06-19T12:24:22.6234957Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:22.6238270Z [12:24:22.623] INFO (asciidoctor): possible invalid reference: erp-settings-day-book-entry
2023-06-19T12:24:22.6238695Z     file: modules/ROOT/pages/Day_Book_Entry.adoc
2023-06-19T12:24:22.6239362Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:23.8808964Z [12:24:23.880] WARN (asciidoctor): unterminated comment block
2023-06-19T12:24:23.8810075Z     file: modules/ROOT/pages/changelogs/sunmi-changelog.adoc:27
2023-06-19T12:24:23.8810563Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:23.9660578Z [12:24:23.965] WARN (asciidoctor): list item index: expected 3, got 1
2023-06-19T12:24:23.9661208Z     file: modules/ROOT/pages/changelogs-new/restaurant-poynt-changelog-new2023.adoc:77
2023-06-19T12:24:23.9661601Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:23.9672082Z [12:24:23.966] WARN (asciidoctor): list item index: expected 4, got 2
2023-06-19T12:24:23.9672958Z     file: modules/ROOT/pages/changelogs-new/restaurant-poynt-changelog-new2023.adoc:78
2023-06-19T12:24:23.9673455Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:23.9679103Z [12:24:23.967] WARN (asciidoctor): list item index: expected 5, got 3
2023-06-19T12:24:23.9679598Z     file: modules/ROOT/pages/changelogs-new/restaurant-poynt-changelog-new2023.adoc:79
2023-06-19T12:24:23.9680088Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/release-note (refname: main)
2023-06-19T12:24:24.6687946Z [12:24:24.667] WARN (asciidoctor): no callout found for <1>
2023-06-19T12:24:24.6688913Z     file: modules/ROOT/pages/manage-profile.adoc:52
2023-06-19T12:24:24.6689393Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:24.6691047Z [12:24:24.668] WARN (asciidoctor): no callout found for <2>
2023-06-19T12:24:24.6691492Z     file: modules/ROOT/pages/manage-profile.adoc:53
2023-06-19T12:24:24.6691963Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:24.6698195Z [12:24:24.669] WARN (asciidoctor): no callout found for <3>
2023-06-19T12:24:24.6698697Z     file: modules/ROOT/pages/manage-profile.adoc:54
2023-06-19T12:24:24.6699661Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:25.4999564Z [12:24:25.499] ERROR (asciidoctor): target of xref not found: platform-orders/view-accepted-orders-pos.adoc
2023-06-19T12:24:25.5000597Z     file: modules/ROOT/pages/platform-orders/platform-orders-nymbl-retail.adoc
2023-06-19T12:24:25.5001589Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:25.5024440Z [12:24:25.501] ERROR (asciidoctor): target of xref not found: restaurant-web-um:ROOT:View_Acptd_Odr.adoc
2023-06-19T12:24:25.5025211Z     file: modules/ROOT/pages/platform-orders/platform-orders-nymbl-retail.adoc
2023-06-19T12:24:25.5026071Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:25.5056381Z [12:24:25.505] ERROR (asciidoctor): target of xref not found: platform-orders/view-accepted-orders-pos.adoc
2023-06-19T12:24:25.5057027Z     file: modules/ROOT/pages/platform-orders/platform-orders-nymbl-retail.adoc
2023-06-19T12:24:25.5057724Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:25.8594316Z [12:24:25.854] WARN (asciidoctor): no callout found for <1>
2023-06-19T12:24:25.8595334Z     file: modules/ROOT/pages/setup-customize/location-settings.adoc:101
2023-06-19T12:24:25.8595889Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:25.8596573Z [12:24:25.855] WARN (asciidoctor): no callout found for <2>
2023-06-19T12:24:25.8597026Z     file: modules/ROOT/pages/setup-customize/location-settings.adoc:102
2023-06-19T12:24:25.8597515Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:25.8597888Z [12:24:25.857] WARN (asciidoctor): no callout found for <3>
2023-06-19T12:24:25.8598386Z     file: modules/ROOT/pages/setup-customize/location-settings.adoc:103
2023-06-19T12:24:25.8598884Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:25.8599237Z [12:24:25.858] WARN (asciidoctor): no callout found for <4>
2023-06-19T12:24:25.8599738Z     file: modules/ROOT/pages/setup-customize/location-settings.adoc:120
2023-06-19T12:24:25.8600311Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:26.2429357Z [12:24:26.242] ERROR (asciidoctor): target of xref not found: platform-orders/view-accepted-orders-pos.adoc
2023-06-19T12:24:26.2430235Z     file: modules/ROOT/pages/platform-orders/retail/accept-orders-retail-pos.adoc
2023-06-19T12:24:26.2430850Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:26.2448749Z [12:24:26.244] ERROR (asciidoctor): target of xref not found: restaurant-web-um:ROOT:View_Acptd_Odr.adoc
2023-06-19T12:24:26.2449306Z     file: modules/ROOT/pages/platform-orders/retail/accept-orders-retail-pos.adoc
2023-06-19T12:24:26.2449838Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:26.2478824Z [12:24:26.247] ERROR (asciidoctor): target of xref not found: platform-orders/view-accepted-orders-pos.adoc
2023-06-19T12:24:26.2479460Z     file: modules/ROOT/pages/platform-orders/retail/accept-orders-retail-pos.adoc
2023-06-19T12:24:26.2479945Z     source: https://dev.azure.com/webdevchk/Support-Docs/_git/qr-order-sm (refname: main)
2023-06-19T12:24:32.2947463Z ##[section]Finishing: Generate Antora site
